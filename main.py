from genericpath import exists
import sys
import operator
import datetime
import os.path
from os import path
from icecream import ic
#from guppy import hpy; h=hpy()

#Taking Space Separated Multiple Inputs
def multiple_input1():
    a,b = map(int,input().split())
    ic(a)
    ic(b)

def multiple_input2():
    arr = list(map(int,input().split()))
    ic(arr)

#Accessing Index and Value at the Same Time
def accessing_index_and_value_same_time():
    arr = [2,4,6,3,8,10]
    for index ,value in enumerate(arr):
        print(f"At index {index} The value Is -> {value}")

#Check Memory Usage

def check_memory_usage():
    a = 20
    ic(sys.getsizeof(a))

#Prints the Unique ID of a Variable

def prints_unique_id_of_a_variable():
    a = 20
    name =  "Yusuf"

    ic(id(a))
    ic(id(name))
    #print(h.heap())

# Check for Anagram

def check_anagram(first_word,second_word):
    return sorted(first_word) == sorted(second_word)


#Merge Two Dictionaries

def merge_two_dictionaries():
    basic_information = {"name":['karl','Lary'],"mobile":["0134567894","0123456789"]}
    academic_information = {"grade":["A","B"]}
    details = dict()

    details = {key: value for data in (basic_information,academic_information)for key,value in data.items()}
    ic(details)

    details = {**basic_information,**academic_information}
    ic(details)

    details = basic_information.copy()
    details.update(academic_information)
    ic(details)

# Checking if a File Exists
def check_for_file():
    ic("File exists: ",path.exists("text.txt"))


#Square of all numbers in a given range
def square_of_all_number_in_a_given_range():
    n = 6
    squares = [i**2 for i in range(1,n+1)]
    range(1,7) 
    ic(squares)

#Converting two lists into a dictionary

def convert_two_list_into_dictionary():
    list1 = [ 'karl','lary','keera']
    list2 = [500,600,700]

    dictt0 = dict(zip(list1,list2))
    ic(dictt0)

#Sorting a list of strings
def sorting_a_list_of_strings():
    list1 = ["yusuf","hakan","mihrimah","aras","arya"]
    list1.sort()
    ic(list1)

#List Comprehension With if and Else

def list_comprehension_with_if_else():
    ic(['FizzBuzz' if i%3 == 0 and i%5 == 0 else 'Fizz' if i%3==0 else 'Buzz' if i%5==0 else i for i in range(1,20)])

#Adding Elements of Two Lists

def adding_elements_of_two_list():
    maths = [59,60,70,80]
    physics = [20,30,40,50]

    list1 = [x+y for x,y in zip(maths,physics)]
    ic(list1)

#Sorting a list of Dictionaries

def sorting_a_list_of_dictionoaries():
    start = datetime.datetime.now()
    dict1 = [
    {"Name":"Karl",
    "Age":25},
    {"Name":"Lary",
    "Age":39},
    {"Name":"Nina",
    "Age":35}
    ]

    dict1.sort(key=lambda item: item.get("Age"))
    ic(dict1)
    ic(datetime.datetime.now()-start)

#Checking For Substrings In a String of List

def checking_for_substrings_in_a_string_of_list():
    addresses = [
    "12/45 Elm street",
    '34/56 Clark street',
    '56,77 maple street',
    '17/45 Elm street'
    ]

    street = 'Elm street'
    for i in addresses:
        if street in i:
            ic(i)

#Formating a String
def formating_a_string():
    name = "yusuf"
    age = 29

    #method1
    print("My name is " + name + ", an I am " +str(age)+ " years old. ")
    #method2
    print(f"My name is {name},and I am {age} years old")
    #method3
    print("My name is {}, and I am {} years old".format(name, age))

#Error Handling
def error_handling():
    try:
        ftpr = open("text.txt",'r')
    except IOError:
        print("File not Found")
    else:
        print("The file opened successfully")    
        ftpr.close()

#Most Frequent in a List
def most_frequent_in_a_list(nums):
    return max(set(nums),key = nums.count)

#Calculator Without if-else

def calculator_without_if_else():
    action = {
    "+" : operator.add,
    "-" : operator.sub,
    "/" : operator.truediv,
    "*" : operator.mul,
    "**" : pow
    }
    ic(action['*'](5,5))

#Chained Function Call
def chained_function_call():
    def add(a,b):
        return a+b
    def sub(a,b):
        return a-b
    a,b= 9,6
    ic((sub if a > b else add)(a,b))

#Swap Values
def swap_values():
    a,b = 5,7

    #Method1 
    b,a = a,b

    #method2
    def swap(a,b):
        return b,a
    ic(swap(a,b))

#Finding Duplicates

def has_duplicates(lst):
    return len(lst) != len(set(lst))


if __name__ == "__main__":
    globals()[sys.argv[1]]()
    ic(check_anagram("silent","listen"))
    ic(check_anagram("ginger","danger"))
    numbers = [1,2,3,4,5,5]
    ic(most_frequent_in_a_list(numbers))
    x = [1,2,2,4,3,5]
    ic(has_duplicates(x))